package com.fl0w3r.jumpme.database

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.fl0w3r.jumpme.entity.QuizItem
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.IOException

class QuizDatabaseTest {

    private lateinit var quizDao: QuizDao
    private lateinit var testDatabase: QuizDatabase

    @Before
    fun setUp() {
        // Get an test instance of the database and the data access object
        val context = ApplicationProvider.getApplicationContext<Context>()
        testDatabase = QuizDatabase.getDatabase(context)
        quizDao = testDatabase.quizDao
    }

    @After
    @Throws(IOException::class)
    fun tearDown() {
        testDatabase.close()
    }

    @Test
    @Throws(Exception::class)
    fun testCreateQuizItem() {
        runBlocking {
            val testQuiz = QuizItem(
                quiz_id = 1,
                question = "How are you?",
                option1 = "option1",
                option2 = "option2",
                option3 = "option3",
                option4 = "option4",
                category = "New",
                answer = 1
            )

            quizDao.insertQuiz(testQuiz)

            val insertedQuiz = quizDao.getQuiz(1)
            Assert.assertEquals(testQuiz, insertedQuiz)
        }
    }

    @Test
    @Throws(Exception::class)
    fun testRemoveAllQuizItems(){
        runBlocking {
            val testQuiz = QuizItem(
                quiz_id = 1,
                question = "How are you?",
                option1 = "option1",
                option2 = "option2",
                option3 = "option3",
                option4 = "option4",
                category = "New",
                answer = 1
            )

            quizDao.insertQuiz(testQuiz)

            quizDao.deleteAll()
            val allQuiz = quizDao.getAllQuiz()
            Assert.assertEquals(allQuiz.size, 0)
        }
    }
}