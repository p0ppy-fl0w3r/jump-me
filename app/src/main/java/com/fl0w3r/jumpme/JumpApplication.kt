package com.fl0w3r.jumpme

import android.app.Application
import timber.log.Timber

class JumpApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())
    }
}