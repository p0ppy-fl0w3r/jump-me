package com.fl0w3r.jumpme.utils

object Constants {
    val WELCOME_MESSAGES = listOf(
        "Are you ready to take a quiz?",
        "So, you're smart? Let's test!",
        "How far can you jump?",
        "How's your day been so far?",
        "Exercise your brain!",
        "Free knowledge ahead."
    )

    // Keys for instance state in GameFragment
    const val CURRENT_QUESTION = "Current question"
    const val CURRENT_SCORE = "Current score"
    const val CURRENT_LIST = "Current list"


}