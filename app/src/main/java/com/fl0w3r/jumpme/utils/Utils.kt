package com.fl0w3r.jumpme.utils

import com.fl0w3r.jumpme.entity.QuizItem
import kotlin.random.Random

object Utils {

    fun getRandomQuiz(list: List<QuizItem>, size: Int): List<QuizItem> {
        val indexList = mutableListOf<Int>()
        val listLen = list.size
        if (listLen < size) {
            return list.shuffled()
        }
        for (i in 0..size) {
            var randomIndex = Random.nextInt(0, listLen)
            while (indexList.contains(randomIndex)) {
                randomIndex = Random.nextInt(0, listLen)
            }
        }

        return indexList.map { list[it] }
    }

}