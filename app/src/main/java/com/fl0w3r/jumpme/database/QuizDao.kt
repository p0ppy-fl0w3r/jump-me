package com.fl0w3r.jumpme.database

import androidx.room.*
import com.fl0w3r.jumpme.entity.QuizItem

@Dao
interface QuizDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertQuiz(quizItem: QuizItem)

    @Delete
    suspend fun removeQuiz(quizItem: QuizItem)

    @Query("SELECT * FROM QuizItem")
    suspend fun getAllQuiz():List<QuizItem>

    @Query("SELECT * FROM QuizItem WHERE quiz_id=:id")
    suspend fun getQuiz(id:Long):QuizItem

    @Query("SELECT * FROM QuizItem WHERE category=:category")
    suspend fun getAllQuizFromCategory(category: String): List<QuizItem>

    @Query("DELETE FROM QuizItem")
    suspend fun deleteAll()

}