package com.fl0w3r.jumpme.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.fl0w3r.jumpme.entity.QuizItem

@Database(entities = [QuizItem::class], version = 1, exportSchema = false)
abstract class QuizDatabase : RoomDatabase() {
    abstract val quizDao: QuizDao

    companion object {
        private lateinit var INSTANCE: QuizDatabase

        fun getDatabase(context: Context): QuizDatabase {
            synchronized(this) {
                if (!::INSTANCE.isInitialized) {
                    INSTANCE =
                        Room.databaseBuilder(context, QuizDatabase::class.java, "quiz_database")
                            .fallbackToDestructiveMigration()
                            .build()
                }
            }
            return INSTANCE
        }

    }
}