package com.fl0w3r.jumpme.repository

import com.fl0w3r.jumpme.database.QuizDatabase
import com.fl0w3r.jumpme.entity.QuizItem

class QuizRepository(private val quizDatabase: QuizDatabase) {

    private val mDatabase = quizDatabase.quizDao

    suspend fun insertQuiz(quizItem: QuizItem){
        mDatabase.insertQuiz(quizItem)
    }

    suspend fun removeQuiz(quizItem: QuizItem){
        mDatabase.removeQuiz(quizItem)
    }

    suspend fun getAllQuiz():List<QuizItem>{
        return mDatabase.getAllQuiz()
    }

    suspend fun getQuiz(id:Long): QuizItem{
        return mDatabase.getQuiz(id)
    }

    suspend fun getAllQuizFromCategory(category: String): List<QuizItem>{
        return mDatabase.getAllQuizFromCategory(category)
    }

    suspend fun deleteAll(){
        mDatabase.deleteAll()
    }

}