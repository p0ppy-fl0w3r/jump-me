package com.fl0w3r.jumpme.game

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.fl0w3r.jumpme.R
import com.fl0w3r.jumpme.databinding.FragmentGameCompleteBinding

class GameCompleteFragment : Fragment() {

    private lateinit var binding: FragmentGameCompleteBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_game_complete, container, false)

        val playerScore = GameCompleteFragmentArgs.fromBundle(requireArguments()).score
        binding.scoreText.text = "Your score is $playerScore"

        return binding.root
    }

}