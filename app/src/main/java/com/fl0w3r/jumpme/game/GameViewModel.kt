package com.fl0w3r.jumpme.game

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.fl0w3r.jumpme.database.QuizDatabase
import com.fl0w3r.jumpme.entity.QuizItem
import com.fl0w3r.jumpme.repository.QuizRepository
import com.fl0w3r.jumpme.utils.Utils
import kotlinx.coroutines.launch

class GameViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = QuizRepository(QuizDatabase.getDatabase(application))

    private val _quizData = MutableLiveData<List<QuizItem>?>()
    val quizData: LiveData<List<QuizItem>?>
        get() = _quizData

    // TODO add category filters
    fun getData() {
        viewModelScope.launch {
            populate()
            val allData = repository.getAllQuiz()
            // TODO show error is the list was empty
            // TODO add option to change the size of the quiz
            if (!allData.isNullOrEmpty()) {
                _quizData.value = Utils.getRandomQuiz(allData, 10)
            }
        }
    }

    fun doneGetData() {
        _quizData.value = null
    }

    // TRIAL
    // TODO Remove this when actual data is inserted
    private suspend fun populate() {

        repository.deleteAll()
        repository.insertQuiz(
            QuizItem(
                1,
                "How are you doing?",
                "General",
                "Nice",
                "Good",
                "Fine",
                "Super!",
                4
            )
        )
        repository.insertQuiz(
            QuizItem(
                2,
                "Are you fine?",
                "No",
                "Yes",
                "Who's asking?",
                "Why?",
                "Super!",
                2
            )
        )

    }

}