package com.fl0w3r.jumpme.game

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.fl0w3r.jumpme.R
import com.fl0w3r.jumpme.databinding.FragmentGameBinding
import com.fl0w3r.jumpme.entity.ParcelQuizItem
import com.fl0w3r.jumpme.entity.QuizItem
import com.fl0w3r.jumpme.utils.Constants
import timber.log.Timber


class GameFragment : Fragment() {

    private lateinit var binding: FragmentGameBinding
    private lateinit var viewModel: GameViewModel

    private var currentQuestion = 0
    private lateinit var questionLists: List<QuizItem>

    private var score = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        viewModel = ViewModelProvider(this)[GameViewModel::class.java]

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_game, container, false)

        if (savedInstanceState != null) {
            score = savedInstanceState.getInt(Constants.CURRENT_SCORE)
            currentQuestion = savedInstanceState.getInt(Constants.CURRENT_QUESTION)
            questionLists =
                savedInstanceState.getParcelableArrayList<ParcelQuizItem>(Constants.CURRENT_LIST)
                    ?.map {
                        QuizItem(
                            it.quiz_id,
                            it.question,
                            it.category,
                            it.option1,
                            it.option2,
                            it.option3,
                            it.option4,
                            it.answer
                        )
                    }!!
        }

        if (!::questionLists.isInitialized) {
            viewModel.getData()
        }

        viewModel.quizData.observe(viewLifecycleOwner) {
            if (it != null) {
                questionLists = it
                // TODO show error when the list is empty

                viewModel.doneGetData()
                setQuestions(currentQuestion)
            }
        }

        binding.submitButton.setOnClickListener {
            checkAnswer()
            nextQuestion()
        }

        return binding.root
    }

    private fun setQuestions(index: Int) {
        if (::questionLists.isInitialized) {
            binding.optionsGroup.clearCheck()
            val quizItem = questionLists[index]
            binding.questionText.text = quizItem.question
            binding.option1.text = quizItem.option1
            binding.option2.text = quizItem.option2
            binding.option3.text = quizItem.option3
            binding.option4.text = quizItem.option4
        }
    }

    private fun checkAnswer() {
        val userSelected = when (binding.optionsGroup.checkedRadioButtonId) {
            binding.option1.id -> 1
            binding.option2.id -> 2
            binding.option3.id -> 3
            binding.option4.id -> 4
            else -> 0
        }
        if (userSelected != 0) {
            score += if (questionLists[currentQuestion].answer == userSelected) 1 else 0
        }
    }

    private fun nextQuestion() {
        currentQuestion++
        if (currentQuestion >= questionLists.size) {
            findNavController().navigate(GameFragmentDirections.gameToComplete(score))
        } else {
            setQuestions(currentQuestion)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(Constants.CURRENT_QUESTION, currentQuestion)
        outState.putInt(Constants.CURRENT_SCORE, score)
        outState.putParcelableArrayList(
            Constants.CURRENT_LIST,
            ArrayList(questionLists.map { it.asParcel() })
        )
    }
}