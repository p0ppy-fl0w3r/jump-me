package com.fl0w3r.jumpme

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.fl0w3r.jumpme.databinding.ActivityMainBinding
// TODO add the following attributes in about
/*
* <a href='https://www.freepik.com/vectors/background'>Background vector created by iconicbestiary - www.freepik.com</a>
* <a href='https://www.freepik.com/vectors/people'>People vector created by stories - www.freepik.com</a>
* */


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        val navController = findNavController(R.id.navHostFragment)
        val drawerLayout = binding.drawerLayout

        setSupportActionBar(binding.toolbar)
        binding.navDrawer.setupWithNavController(navController)

        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout)
        NavigationUI.setupWithNavController(binding.navDrawer, navController)

        navController.addOnDestinationChangedListener { controller, destination, _ ->
            if (controller.graph.startDestination != destination.id) {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                binding.toolbar.visibility = Toolbar.VISIBLE
                binding.navButton.visibility = ImageView.GONE
            } else {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                binding.toolbar.visibility = Toolbar.GONE
                binding.navButton.visibility = ImageView.VISIBLE
            }
        }

        binding.navButton.setOnClickListener {
            drawerLayout.open()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.navHostFragment)
        return NavigationUI.navigateUp(navController, binding.drawerLayout)
    }
}