package com.fl0w3r.jumpme.title

import android.graphics.drawable.AnimatedVectorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.fl0w3r.jumpme.utils.Constants
import com.fl0w3r.jumpme.R
import com.fl0w3r.jumpme.databinding.FragmentTitleBinding

class TitleFragment : Fragment() {
    
    private lateinit var binding: FragmentTitleBinding
    
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_title, container, false)
        binding.welcomeMessageText.text = Constants.WELCOME_MESSAGES.random()

        val animVector = binding.thinkImage.drawable as AnimatedVectorDrawable
        animVector.start()

        binding.playButton.setOnClickListener {
            findNavController().navigate(TitleFragmentDirections.titleToGame())
        }

        return binding.root
    }


}