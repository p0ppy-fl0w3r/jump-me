package com.fl0w3r.jumpme.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ParcelQuizItem(
    var quiz_id: Long,
    var question: String,
    var category:String,
    var option1: String,
    var option2: String,
    var option3: String,
    var option4: String,
    var answer: Int,
) : Parcelable
