package com.fl0w3r.jumpme.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class QuizItem(
    @PrimaryKey
    var quiz_id: Long,
    var question: String,
    var category: String,
    var option1: String,
    var option2: String,
    var option3: String,
    var option4: String,
    var answer: Int,
) {
    fun getAllOptions(): List<String> {
        return listOf(option1, option2, option3, option4)
    }

    fun asParcel(): ParcelQuizItem {
        return ParcelQuizItem(
            quiz_id,
            question,
            category,
            option1,
            option2,
            option3,
            option4,
            answer
        )
    }

    fun getCorrectAnswer(index: Int): String?{
        return when(index){
            1 -> option1
            2 -> option2
            3 -> option3
            4 -> option4
            else -> null // Options should not exceed 4
        }
    }
}
