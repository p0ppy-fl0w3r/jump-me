# Jump me
##### Quizzes, Trivia and more.

A simple game for android, made in kotlin. Requires API level 27 and above. To play, clone this repo and use the the following command for linux: 
 ```bash
 ./gradlew assembleDebug
```
or for windows:
```cmd
gradlew assembleDebug
```

The apk file will be located in /JumpMe/app/build/outputs/apk/debug directory.
Enjoy :)
